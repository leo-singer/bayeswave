/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */


/***************************  REQUIRED LIBRARIES  ***************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

/* ============================  MAIN PROGRAM  ============================ */

int main(int argc, char* argv[])
{

  FILE *waveFile=NULL;
  char filename[1024];

  int i,n;

  int N = 522240;
  int M = 100;
  
  int T;// = 128;
  double norm;// = 1./((double)T/2.);

  double f,d,Sn,Sl;
  

  double **wave = NULL;
  double *freq  = NULL;
  
  wave = malloc(N*sizeof(double*));
  freq = malloc(N*sizeof(double));

  for(n=0; n<N; n++) wave[n] = malloc(M*sizeof(double));

  for(n=0; n<N; n++) for(i=0; i<M; i++) wave[n][i] = 0.0;

  for(i=100; i<200; i++)
  {

    //sprintf(filename,"BayesWave_PSD_%s_clean_psd_%i.dat.0",argv[1],i);
    if(argc==2)sprintf(filename,"waveforms/%s_clean_psd_%i.dat.0",argv[1],i);
    else       sprintf(filename,"waveforms/clean_psd_%i.dat.0",i);

    waveFile=fopen(filename,"r");
    if(waveFile==NULL)
    {
      fprintf(stdout,"%s not found\n",filename);
      fprintf(stdout,"check runName\n");
      fprintf(stdout,"check that --gnuplot flag was enabled in BayesWave run\n\n");
      fprintf(stdout,"Usage: bayeswave_to_lalpsd runName(optional)\n");
      return 0;
    }

    for(n=0; n<N; n++)
    {

      fscanf(waveFile,"%lg %lg %lg %lg",&f,&d,&Sn,&Sl);

      wave[n][i-100] = Sn+Sl;
      freq[n] = f;

    }

    //find Tobs for segment and compute normalization to map from BW to LALInf
    if(i==100)
    {
      T = (int)round(1.0/(freq[1]-freq[0]));
      norm = 1./((double)T/2.);
      fprintf(stdout,"segment length %i s\n",T);
    }
    
    fclose(waveFile);
  }

  
  if(argc==2)sprintf(filename,"%s_IFO0_asd_median.dat",argv[1]);
  else       sprintf(filename,"IFO0_asd_median.dat");

  FILE *outfile = fopen("psd_intervals.dat","w");
  FILE *outfile2 = fopen("psd_median.dat","w");
  FILE *outfile3 = fopen(filename,"w");

  if(outfile3==NULL)printf("Failed to open %s\n",filename);  
 
  double median,upper90,lower90,upper50,lower50;
  for(n=0; n<N; n++)
  {
    gsl_sort(wave[n], 1, M);
    
    median = gsl_stats_median_from_sorted_data    (wave[n], 1, M);
    upper90 = gsl_stats_quantile_from_sorted_data (wave[n], 1, M, 0.05);
    lower90 = gsl_stats_quantile_from_sorted_data (wave[n], 1, M, 0.95);
    upper50 = gsl_stats_quantile_from_sorted_data (wave[n], 1, M, 0.25);
    lower50 = gsl_stats_quantile_from_sorted_data (wave[n], 1, M, 0.75);
    
    fprintf(outfile,"%lg %lg %lg %lg %lg %lg\n",freq[n],median*norm,lower50*norm,upper50*norm,lower90*norm,upper90*norm);
    fprintf(outfile2,"%.16g %.16g\n",freq[n],sqrt(median*norm));
    fprintf(outfile3,"%.16g %.16g\n",freq[n],sqrt(median*norm));
  }
  fclose(outfile);
  

  
  
  return 0;
}

