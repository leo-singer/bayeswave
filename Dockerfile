FROM containers.ligo.org/lscsoft/lalsuite/lalsuite-v6.49a:stretch
ARG version
RUN echo "Building bayeswave"
MAINTAINER James Alexander Clark <james.clark@ligo.org>

# Dependencies
RUN apt-get update && apt-get install --assume-yes \
      build-essential \
      pkg-config \
      python-pip \
      git \
      python-ligo-lw

RUN python -m pip install --upgrade setuptools

# Copy and build BayesWave
WORKDIR /
# This seems very hacky but will get us git verion info in the configure.ac:
COPY .git /
COPY install.sh /
COPY src /src
copy etc/bayeswave-user-env.sh /etc/bayeswave-user-env.sh
COPY BayesWaveUtils /BayesWaveUtils
RUN sh install.sh /opt/lscsoft/bayeswave 
COPY test /opt/lscsoft/bayeswave/test
RUN rm -rf .git install.sh /src /BayesWaveUtils /etc/bayeswave-user-env.sh
RUN mkdir -p /cvmfs /hdfs /hadoop

# BayesWave env
ENV PATH /opt/lscsoft/bayeswave/bin:${PATH}
ENV LD_LIBRARY_PATH /opt/lscsoft/bayeswave/lib:${LD_LIBRARY_PATH}
ENV PYTHONPATH /opt/lscsoft/bayeswave/lib/python2.7/site-packages:${PYTHONPATH}

ENTRYPOINT ["/bin/bash"]




